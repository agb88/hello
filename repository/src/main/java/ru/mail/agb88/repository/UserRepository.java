package ru.mail.agb88.repository;

import org.apache.log4j.Logger;
import ru.mail.agb88.repository.model.User;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Created by user on 26.06.2017.
 */
public class UserRepository {
    private static final Logger logger = Logger.getLogger(UserRepository.class);
    private static UserRepository instance;
    private String[] arr = new String[4];
    private int loginColumn;
    private int passwordColumn;
    private int nameColumn;
    private int roleColumn;

    public static UserRepository getInstance() {
        if (instance == null) {
            instance = new UserRepository();
        }
        return instance;
    }

    public List<User> getUsers() {

        List<User> users = new ArrayList<>();
        File file = new File(getClass().getClassLoader().getResource("users.txt").getFile());
        try (Scanner scanner = new Scanner(file)) {

            // Шапка таблицы
            if (scanner.hasNextLine()) {
                String s = scanner.nextLine();
                arr = s.split("\\|");

                if (logger.isDebugEnabled()) {
                    logger.debug(Arrays.toString(arr) + " " + arr.length);
                }

                // Ищем номера столбиков
                for (int i = 0; i < arr.length; i++) {
                    logger.info("i = " + i);
                    switch (arr[i]) {
                        case "LOGIN": {
                            loginColumn = i;
                            break;
                        }
                        case "PASSWORD": {
                            passwordColumn = i;
                            break;
                        }
                        case "NAME": {
                            nameColumn = i;
                            break;
                        }
                        case "Role": {
                            roleColumn = i;
                            break;
                        }
                        default:
                            break;
                    }
                }
            }

            //Пишем пользователей
            while (scanner.hasNextLine()) {
                User user = new User();
                arr = scanner.nextLine().split(","); // Получаем массив первой строки (юзер)

                if (logger.isDebugEnabled()) {
                    logger.debug(Arrays.toString(arr) + " " + loginColumn + " " + passwordColumn + " " + nameColumn + " " + roleColumn);
                }

                user.setLogin(arr[loginColumn]);
                user.setPassword(arr[passwordColumn]);
                user.setName(arr[nameColumn]);
                user.setRole(arr[roleColumn]);
                users.add(user);
            }

        } catch (FileNotFoundException e) {
            logger.error("File not found", e);
        }
        return users;
    }
}
