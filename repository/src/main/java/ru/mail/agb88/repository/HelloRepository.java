package ru.mail.agb88.repository;

import org.apache.log4j.Logger;

/**
 * Created by user on 26.06.2017.
 */
public class HelloRepository {
    private static final Logger logger = Logger.getLogger(HelloRepository.class);
    private static HelloRepository instance;

    public static HelloRepository getInstance() {
        if (instance == null) {
            instance = new HelloRepository();
        }
        return instance;
    }

    public void getHello() {
        if (logger.isDebugEnabled()) {
            logger.debug("Hello World from Repository");
        }
    }
}
