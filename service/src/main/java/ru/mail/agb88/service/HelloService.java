package ru.mail.agb88.service;

import org.apache.log4j.Logger;
import ru.mail.agb88.repository.HelloRepository;

/**
 * Created by user on 26.06.2017.
 */
public class HelloService {
    private static final Logger logger = Logger.getLogger(HelloService.class);
    private static HelloService instance;

    public static HelloService getInstance() {
        if (instance == null) {
            instance = new HelloService();
        }
        return instance;
    }

    public void getHello() {
        HelloRepository.getInstance().getHello();

        if (logger.isDebugEnabled()) {
            logger.debug("Hello World from Service");
        }
    }
}
