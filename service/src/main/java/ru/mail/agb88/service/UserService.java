package ru.mail.agb88.service;


import ru.mail.agb88.repository.UserRepository;
import ru.mail.agb88.repository.model.User;
import ru.mail.agb88.service.model.UserDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 26.06.2017.
 */
public class UserService {

    private static UserService instance;

    public static UserService getInstance() {
        if (instance == null) {
            instance = new UserService();
        }
        return instance;
    }

    public List<UserDTO> getUsers (){
        List <User> users = UserRepository.getInstance().getUsers();
        List <UserDTO> usersDTO = new ArrayList<>();
        for (User user : users) {
            usersDTO.add(new UserDTO(user.getLogin(), user.getPassword(), user.getName(), user.getRole()));
        }

        return usersDTO;
    }

}
