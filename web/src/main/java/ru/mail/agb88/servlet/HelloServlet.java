package ru.mail.agb88.servlet;

import org.apache.log4j.Logger;
import ru.mail.agb88.service.HelloService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by user on 26.06.2017.
 */
public class HelloServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(HelloServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HelloService.getInstance().getHello();

        if (logger.isDebugEnabled()) {
            logger.debug("Hello World from Servlet");
        }
    }
}
