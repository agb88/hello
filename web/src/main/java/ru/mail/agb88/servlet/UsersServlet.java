package ru.mail.agb88.servlet;

import ru.mail.agb88.service.UserService;
import ru.mail.agb88.service.model.UserDTO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by r2d2 on 27.06.2017.
 */
public class UsersServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        List<UserDTO> users = UserService.getInstance().getUsers();
        writer.write("LOGIN|PASSWORD|NAME|Role");

        for (UserDTO user : users) {
            writer.write("\r\n" + user.getLogin() + " | " + user.getPassword() + " | " + user.getName() + " | " + user.getRole());
        }
    }
}
